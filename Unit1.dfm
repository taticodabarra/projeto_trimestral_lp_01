object Form1: TForm1
  Left = 742
  Top = 172
  Width = 599
  Height = 509
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 248
    Top = 24
    Width = 56
    Height = 21
    Caption = 'S'#233'ries'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Mongolian Baiti'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 104
    Top = 216
    Width = 43
    Height = 16
    Caption = 'Nome'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Mongolian Baiti'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 96
    Top = 248
    Width = 53
    Height = 16
    Caption = 'Genero'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Mongolian Baiti'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 112
    Top = 280
    Width = 34
    Height = 16
    Caption = 'Data'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Mongolian Baiti'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 80
    Top = 312
    Width = 67
    Height = 16
    Caption = 'Atualizar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Mongolian Baiti'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Edit_Nome: TEdit
    Left = 152
    Top = 208
    Width = 249
    Height = 21
    TabOrder = 0
  end
  object Edit_Genero: TEdit
    Left = 152
    Top = 240
    Width = 249
    Height = 21
    TabOrder = 1
  end
  object Edit_Data: TEdit
    Left = 152
    Top = 272
    Width = 249
    Height = 21
    TabOrder = 2
  end
  object ListBoxSeries: TListBox
    Left = 152
    Top = 56
    Width = 249
    Height = 145
    ItemHeight = 13
    TabOrder = 3
  end
  object Btn_Inserir: TButton
    Left = 408
    Top = 56
    Width = 75
    Height = 33
    Caption = 'Inserir'
    TabOrder = 4
    OnClick = Btn_InserirClick
  end
  object Btn_Atualizar: TButton
    Left = 408
    Top = 136
    Width = 75
    Height = 33
    Caption = 'Atualizar'
    TabOrder = 5
    OnClick = Btn_AtualizarClick
  end
  object Btn_Deletar: TButton
    Left = 408
    Top = 96
    Width = 75
    Height = 33
    Caption = 'Deletar'
    TabOrder = 6
    OnClick = Btn_DeletarClick
  end
  object Edit_Atualizar: TEdit
    Left = 152
    Top = 304
    Width = 249
    Height = 21
    TabOrder = 7
  end
end
