unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit_Nome: TEdit;
    Edit_Genero: TEdit;
    Edit_Data: TEdit;
    ListBoxSeries: TListBox;
    Btn_Inserir: TButton;
    Btn_Atualizar: TButton;
    Btn_Deletar: TButton;
    Edit_Atualizar: TEdit;
    Label5: TLabel;
    procedure Btn_InserirClick(Sender: TObject);
    procedure Btn_DeletarClick(Sender: TObject);
    procedure Btn_AtualizarClick(Sender: TObject);
    

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
 type
    TSeries = class(TObject)
    Nome,Genero,Data: string;
  end;

  var Series: TSeries;
  Arquivo : TextFile;



{$R *.dfm}

procedure TForm1.Btn_InserirClick(Sender: TObject);
begin

  if (Edit_Nome.Text <> '') and (Edit_Genero.Text <> '') and (Edit_Data.Text <> '') then

    begin

      ListBoxSeries.Items.Add(Edit_Nome.Text);
      ListBoxSeries.Items.Add(Edit_Genero.Text);
      ListBoxSeries.Items.Add(Edit_Data.Text);

    end
   Else
 begin
ShowMessage('Campo Incompleto');
 end;

end;

procedure TForm1.Btn_DeletarClick(Sender: TObject);
begin

  ListBoxSeries.DeleteSelected;

end;

procedure TForm1.Btn_AtualizarClick(Sender: TObject);
begin

ListBoxSeries.DeleteSelected;

ListBoxSeries.Items.Add(Edit_Atualizar.Text);

end;

end.
